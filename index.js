const DiscordUnitTest = require('./src/discord')

/**
 * @typedef {*} UnitTestArgs - A set of arguments passed to DiscordUnitTest (not `onReady`)
 * @see {@link DiscordUnitTest}
 */
/**
 * Callback once the DiscordUnitTest is initialized
 * @callback DiscordUnitTest~unitTestReady
 * @param {Object} t - The DiscordUnitTest object
 */

/**
 * Run a set of tests
 * @param {DiscordUnitTest~unitTestReady} func - The function containing all the tests
 * @param {string} comment - A description of all the tests
 * @param {UnitTestArgs} unitTestArgs - all the arguments passed
 * @example
 * const test = require('discunit.js')
 * test(t => {
 *   // Do your unit tests here
 * }, 'Main Test') // UnitTestArgs go after
 */
function test (func, comment, ...unitTestArgs) {
  if (comment) console.log(comment)
  let unitTest = new DiscordUnitTest(...unitTestArgs, func)
  void unitTest
}

module.exports = test
