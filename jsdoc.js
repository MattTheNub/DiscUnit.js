module.exports = {
  plugins: ['plugins/markdown'],
  recurseDepth: 10,
  source: {
    include: ['src/', 'index.js', 'README.md'],
    includePattern: '.+(\\.js(doc|x)|md)?$',
    excludePattern: '(node_modules/|docs)'
  },
  opts: {
    template: 'node_modules/docdash',
    encoding: 'utf8',
    destination: './docs/',
    recurse: true
  },
  markdown: {
    parser: 'gfm',
    hardwrap: true
  }
}
