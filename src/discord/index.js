const UnitTest = require('../standard')
const Discord = require('discord.js')

// Typedefs
/**
 * @typedef {string} Snowflake - A Twitter snowflake, except the epoch is 2015-01-01T00:00:00.000Z
 */
/**
 * @typedef {Object} Client - A discord client
 * @see {@link https://discord.js.org/#/docs/main/stable/class/Client Discord.Client}
 */

/**
 * Unit testing functions for Discord
 * @extends UnitTest
 */
class DiscordUnitTest extends UnitTest {
  /**
   * Initializes a new Discord Unit Test
   * @constructor
   * @param {number} count - The amount of tests to be run
   * @param {string} token - The discord login token for the unit testing bot (**not** the bot you are testing)
   * @param {Snowflake} authorID - The ID of the bot you are testing
   * @param {Snowflake} guildID - The ID of the guild to test in
   * @param {Snowflake} channelID - The ID of the channel to test in
   * @param {DiscordUnitTest~unitTestReady} onReady - The function to call when the unit testing bot is ready
   */
  constructor (count, token, authorID, guildID, channelID, onReady) {
    super(count)
    this.author = authorID
    this.guild = guildID
    this.channel = channelID
    /**
     * @var {Client} - The client used for unit testing
     */
    this.client = new Discord.Client()
    this.client.login(token)
    this.client.on('ready', () => onReady(this))
  }
  /**
   * Test if the next sent message's content is equal to something (30s timeout)
   * @param {string} value - The value to test
   * @param {string} [comment] - A description of the test
   * @example
   * t.messageContentEqualTo('hello', 'Bot should say hello')
   * message.channel.send('hello') // Internal bot being tested
   * // #1 Message Content Equality (Passed) - Bot should say hello
   */
  messageContentEqualTo (value, comment) {
    this.count++
    const { author, channel } = this
    const collector = new Discord.MessageCollector(
      this.client.channels.get(this.channel),
      message => message.author.id === author && message.channel.id === channel,
      { time: 30e3 }
    )
    var done = false
    collector.on('collect', message => {
      done = true
      if (value === message.content) {
        console.log(
          `#${this.count} Message Content Equality (Passed)${
            comment ? ` - ${comment}` : ''
          }`
        )
      } else {
        console.log(
          `#${this.count} Message Content Equality (Failed)${
            comment ? ` - ${comment}` : ''
          }`
        )
        console.log(`Expected ${value} but got ${message.content}`)
        throw new Error(`#${this.count} failed`)
      }
      if (this.count === this.end) process.exit()
    })
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (!done) reject(new Error('Timed out'))
        process.exit(1) // Node doesn't automatically exit the process
      }, 30e3)
      setInterval(() => {
        if (done) resolve(this)
      }, 1e3)
    })
  }
  /**
   *
   * @param {string} comment - A description of the test
   * @param {...Object} options - Test options
   * @param {number} options.index - The index of the embed
   * @param {string} options.key - The key to test
   * @param {*} options.value - The value the key should be equal to
   * @example
   * messageEmbedEqualTo('Test the title', {
   *   embedIndex: 0,
   *   key: 'title',
   *   value: 'Good Embed'
   * })
   * message.channel.send(new Discord.RichEmbed().setTitle('Good Embed')) // Internal bot being tested
   * // #1.0 Message Embed Equality (Passed) - Test the title
   */
  messageEmbedEqualTo (comment, ...options) {
    this.count++
    const { author, channel } = this
    const collector = new Discord.MessageCollector(
      this.client.channels.get(this.channel),
      message => message.author.id === author && message.channel.id === channel,
      { time: 30e3 }
    )
    collector.on('collect', message => {
      for (
        var element = options[0], i = 0;
        i < options.length;
        element = options[i++]
      ) {
        if (message.embeds[element.index][element.key] === element.value) {
          console.log(
            `#${this.count}.${i} Message Embed Equality (Passed)${
              comment ? ` - ${comment}` : ''
            }`
          )
        } else {
          console.log(
            `#${this.count}.${i} Message Embed Equality (Failed)${
              comment ? ` - ${comment}` : ''
            }`
          )
          console.log(
            `Expected ${element.value} but got ${
              message.embeds[element.index][element.key]
            }`
          )
          throw new Error(`#${this.count}.${i} failed`)
        }
      }
      if (this.count === this.end) process.exit()
    })
  }
}

module.exports = DiscordUnitTest
